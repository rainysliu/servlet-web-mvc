package com.itranswarp.learnjava.controller;

import com.itranswarp.learnjava.bean.SignInBean;
import com.itranswarp.learnjava.bean.User;
import com.itranswarp.learnjava.bean.UserBean;
import com.itranswarp.learnjava.framework.GetMapping;
import com.itranswarp.learnjava.framework.ModelAndView;
import com.itranswarp.learnjava.framework.PostMapping;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UserController {

    private static final Map<String, User> userDatabase = new HashMap<>();

    static {
        List<User> users = List.of( //
                new User("ang@qq.com", "ang123", "Ang", "This is ang.", "/static/userImages/Ang.jpg"),
                new User("tom@qq.com", "tom123", "Tom", "This is tom.", "/static/userImages/_DefaultUser.jpg"));
        for (User user : users) {
            userDatabase.put(user.email, user);
        }
    }

    @GetMapping("/signin")
    public ModelAndView signin() {
        return new ModelAndView("/signin.html");
    }

    @PostMapping("/signin")
    public ModelAndView doSignin(SignInBean bean, HttpServletResponse response, HttpSession session)
            throws IOException {
        User user = userDatabase.get(bean.email);
        if (user == null || !user.password.equals(bean.password)) {
            response.setContentType("application/json");
            PrintWriter pw = response.getWriter();
            pw.write("{\"error\":\"Bad email or password\"}");
            pw.flush();
        } else {
            session.setAttribute("user", user);
            response.setContentType("application/json");
            PrintWriter pw = response.getWriter();
            pw.write("{\"result\":true}");
            pw.flush();
        }
        return null;
    }

    @GetMapping("/signout")
    public ModelAndView signout(HttpSession session) {
        session.removeAttribute("user");
        return new ModelAndView("redirect:/");
    }

    @GetMapping("/user/profile")
    public ModelAndView profile(HttpSession session) {
        User user = (User) session.getAttribute("user");
        if (user == null) {
            return new ModelAndView("redirect:/signin");
        }
        return new ModelAndView("/profile.html", "user", user);
    }

    @GetMapping("/register")
    public ModelAndView register() {
        return new ModelAndView("/register.html");
    }

    @PostMapping("/register")
    public ModelAndView doRegister(UserBean bean, HttpServletResponse response) throws IOException {
        User user = new User(bean.email, bean.password, bean.name, bean.description, "/static/userImages/_DefaultUser.jpg");
        userDatabase.put(user.email, user);
        response.setContentType("application/json");
        PrintWriter pw = response.getWriter();
        pw.write("{\"result\":true}");
        pw.flush();
        return null;
    }
}
