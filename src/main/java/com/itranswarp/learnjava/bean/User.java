package com.itranswarp.learnjava.bean;

public class User {

    public String email;
    public String password;
    public String name;
    public String description;
    public String image;

    public User(String email, String password, String name, String description, String image) {
        this.email = email;
        this.password = password;
        this.name = name;
        this.description = description;
        this.image = image;
    }
}
