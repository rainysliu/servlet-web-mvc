package com.itranswarp.learnjava.framework;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.*;


@WebServlet(urlPatterns = "/")
public class DispatcherServlet extends HttpServlet {
    private static final Set<Class<?>> supportedGetParameterTypes = Set.of(int.class, long.class, boolean.class,
            String.class, HttpServletRequest.class, HttpServletResponse.class, HttpSession.class);
    private static final Set<Class<?>> supportedPostParameterTypes = Set.of(HttpServletRequest.class,
            HttpServletResponse.class, HttpSession.class);
    private final Logger logger = LoggerFactory.getLogger(getClass());
    private final Map<String, GetDispatcher> getMappings = new HashMap<>();
    private final Map<String, PostDispatcher> postMappings = new HashMap<>();
    private final List<Class<?>> controllers = new ArrayList<>();
    private ViewEngine viewEngine;

    @Override
    public void init() throws ServletException {
        try {
            this.scanClassesInController();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        logger.info("init {}...", getClass().getSimpleName());
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        for (Class<?> controllerClass : this.controllers) {
            try {
                Object controllerInstance = controllerClass.getConstructor().newInstance();
                // 依次处理每个Method:
                for (Method method : controllerClass.getMethods()) {
                    if (method.getAnnotation(GetMapping.class) != null) {
                        scanGetInControllers(method, controllerInstance);
                    } else if (method.getAnnotation(PostMapping.class) != null) {
                        scanPostInControllers(method, controllerInstance, objectMapper);
                    }
                }
            } catch (ReflectiveOperationException e) {
                throw new ServletException(e);
            }
        }
        this.viewEngine = new ViewEngine(getServletContext());
    }

    private void scanClassesInController() throws ClassNotFoundException {
        File fs = new File("target/classes/com/itranswarp/learnjava/controller");
        File[] fList = fs.listFiles();
        if (fList == null) return;
        for (File f : fList) {
            if (!f.getName().endsWith(".class")) continue;
            String className = "com.itranswarp.learnjava.controller." + f.getName().replace(".class", "");
            logger.info("class:[" + className + "] is found.");
            this.controllers.add(Class.forName(className));
        }
    }

    private void scanGetInControllers(Method method, Object controllerInstance) {
        if (method.getReturnType() != ModelAndView.class && method.getReturnType() != void.class) {
            throw new UnsupportedOperationException(
                    "Unsupported return type: " + method.getReturnType() + " for method: " + method);
        }
        for (Class<?> parameterClass : method.getParameterTypes()) {
            if (!supportedGetParameterTypes.contains(parameterClass)) {
                throw new UnsupportedOperationException(
                        "Unsupported parameter type: " + parameterClass + " for method: " + method);
            }
        }
        String[] parameterNames = Arrays.stream(method.getParameters()).map(Parameter::getName)
                .toArray(String[]::new);
        String path = method.getAnnotation(GetMapping.class).value();
        logger.info("Found GET: {} => {}", path, method);
        this.getMappings.put(path, new GetDispatcher(controllerInstance, method, parameterNames,
                method.getParameterTypes()));
    }

    private void scanPostInControllers(Method method, Object controllerInstance, ObjectMapper objectMapper) {
        if (method.getReturnType() != ModelAndView.class && method.getReturnType() != void.class) {
            throw new UnsupportedOperationException(
                    "Unsupported return type: " + method.getReturnType() + " for method: " + method);
        }
        Class<?> requestBodyClass = null;
        for (Class<?> parameterClass : method.getParameterTypes()) {
            if (!supportedPostParameterTypes.contains(parameterClass)) {
                if (requestBodyClass == null) {
                    requestBodyClass = parameterClass;
                } else {
                    throw new UnsupportedOperationException("Unsupported duplicate request body type: "
                            + parameterClass + " for method: " + method);
                }
            }
        }
        String path = method.getAnnotation(PostMapping.class).value();
        logger.info("Found POST: {} => {}", path, method);
        this.postMappings.put(path, new PostDispatcher(controllerInstance, method,
                method.getParameterTypes(), objectMapper));
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException {
        try {
            process(req, res, this.getMappings);
        } catch (ReflectiveOperationException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException {
        try {
            process(req, res, this.postMappings);
        } catch (ReflectiveOperationException e) {
            e.printStackTrace();
        }
    }

    private void process(HttpServletRequest req, HttpServletResponse resp, Map<String, ? extends AbstractDispatcher> dispatcherMap) throws IOException, ReflectiveOperationException {
        resp.setContentType("text/html");
        resp.setCharacterEncoding("UTF-8");
        String path = req.getRequestURI().substring(req.getContextPath().length());
        // 根据路径查找GetDispatcher:
        AbstractDispatcher dispatcher = dispatcherMap.get(path);
        if (dispatcher == null) {
            // 未找到返回404:
            resp.sendError(404);
            return;
        }
        // 调用Controller方法获得返回值:
        ModelAndView mv = null;
        try {
            mv = dispatcher.invoke(req, resp);
        } catch (InvocationTargetException | IllegalAccessException e) {
            e.printStackTrace();
        }
        // 允许返回null:
        if (mv == null) {
            return;
        }
        // 允许返回`redirect:`开头的view表示重定向:
        if (mv.view.startsWith("redirect:")) {
            resp.sendRedirect(mv.view.substring(9));
            return;
        }
        // 将模板引擎渲染的内容写入响应:
        PrintWriter pw = resp.getWriter();
        this.viewEngine.render(mv, pw);
        pw.flush();
    }
}
